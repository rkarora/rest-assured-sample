package com.opeaklabs.web.filecounter.dao;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;



public class FileDao {
	
	
	public int getCount() {
		int count = 0;
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		PrintWriter printWriter = null;
		
		try {
			
			File f = new File("FileCounter.initial");
			if (!f.exists()) {
				
				f.createNewFile();
				printWriter = new PrintWriter (new FileWriter(f));
				printWriter.println(0);
			}
			
				if (printWriter != null) {
				printWriter.close();
				
				
				}
				
				fileReader = new FileReader(f);
				bufferedReader = new BufferedReader(fileReader);
				String initail = bufferedReader.readLine();
				count = Integer.parseInt(initail);
				
				
			
		} catch (Exception ex) {
			
			if (printWriter != null) {
				
				printWriter.close();
			}
			
			
		}
		
		return count;
		
	}
	
	public void save(int count ) throws Exception {
		
		FileWriter fileWriter = null;
		PrintWriter printWriter = null;
		
		fileWriter = new FileWriter("FileCounter.initial");
		printWriter = new PrintWriter(fileWriter);
		printWriter.println(count);
		
		if (printWriter != null) {
			printWriter.close();
	}

}
}


