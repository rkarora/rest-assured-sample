package com.opeaklabs.rest.inventory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.QueryParam;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;

import com.opeaklabs.dao.*;
import com.opeaklabs.rest.*;
import com.opeaklabs.util.ToJSON;
//jettison.jason.JASONArray;

@Path("/v2/inventory")
public class V2_inventory {

	/*
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnParts(@QueryParam("brand") String brand) throws Exception {
		return Response.status(400).entity("Error: please specify brand for this search").build();
	}
	*/
	//Shows how to use QueryParam
	//http://localhost:8080/RestServiceSample/api/v2/inventory?brand=Maker-1
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnBrandParts(@QueryParam("brand") String brand) throws Exception {
		
		String returnString = null;
		JSONArray json = new JSONArray();
		Response rb = null;
//		
		try {

			System.out.println("Brand from V2 = "+brand);
			if (brand == null) {
				
				return Response.status(400).entity("Error: please specify brand for this search").build();
			}
		SQLQueries dao = new SQLQueries();
		json = dao.getBrandParts(brand);
		returnString = json.toString();
		rb = Response.ok(returnString).build();
		
		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		finally {
//			if (conn != null) {
//				conn.close();	
//			}
			
			
		}
		
		
		return rb;
	}
	
	// Shows how to use  PermParam
	// http://localhost:8080/RestServiceSample/api/v2/inventory/Maker-3
	@Path("/{brand}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnBrand(@PathParam("brand") String brand) throws Exception {
		
		String returnString = null;
		JSONArray json = new JSONArray();
		Response rb = null;
//		
		try {

			System.out.println("Brand from V2 = "+brand);
			if (brand == null) {
				
				return Response.status(400).entity("Error: please specify brand for this search").build();
			}
		SQLQueries dao = new SQLQueries();
		json = dao.getBrandParts(brand);
		returnString = json.toString();
		rb = Response.ok(returnString).build();
		
		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		finally {
			
		}
		
		
		return rb;
	}

	// Shows how to use  POST 
	//with APPLICATION_FORM_URLENCODED consume ( for JSON input look at POST method in V3_investory.java
	//http://localhost:8080/RestServiceSample/post.html
		
		@POST
		@Consumes({MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON})
		@Produces(MediaType.APPLICATION_JSON)
		public Response addPcParts(String incomingData) throws Exception {
			
			String returnString = null;
			JSONArray json = new JSONArray();
			Response rb = null;
			SQLQueries dao = new SQLQueries();
//			
			try {

				System.out.println("Incoming Data = "+incomingData);
				ObjectMapper mapper = new ObjectMapper();
				
				ItemEntry itemEntry = mapper.readValue(incomingData,ItemEntry.class);
				
				int http_code = dao.insertIntoPC_parts(itemEntry.PC_PARTS_TITLE, 
						itemEntry.PC_PARTS_CODE, 
						itemEntry.PC_PARTS_MAKER, 
						itemEntry.PC_PARTS_AVAIL, itemEntry.PC_PARTS_DEC); 
				
				if (http_code == 200) {
//					returnString = json.toString();
					returnString = "item inserted";
				}
				else {
					return Response.status(500).entity("Unable to inset record ").build();
				}
			
						
			} catch (Exception e) {
				
				e.printStackTrace();
				return Response.status(500).entity("Server was unable to process your request ").build();
			}
			finally {
				
			}
			
			
			return Response.ok(returnString).build();
		}

	
	
	// Shows how to use Two PermParam
   //http://localhost:8080/RestServiceSample/api/v2/inventory/Maker-4/5
			@Path("/{brand}/{item_number}")
			@GET
			@Produces(MediaType.APPLICATION_JSON)
			public Response returnSecificpeBrand(
					@PathParam("brand") String brand,
					@PathParam("item_number") int item_number) throws Exception {
				
				String returnString = null;
				JSONArray json = new JSONArray();
				Response rb = null;
//				
				try {

					System.out.println("Brand from V2 = "+brand + " item number - " +item_number);
					
//					if (brand == null) {
//						
//						return Response.status(400).entity("Error: please specify brand for this search").build();
//					}
				
				SQLQueries dao = new SQLQueries();
				json = dao.getSpecificPart(brand, item_number);
				returnString = json.toString();
				rb = Response.ok(returnString).build();
				
				
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				finally {
//					if (conn != null) {
//						conn.close();	
//					}
					
					
				}
				
				
				return rb;
			}
	
			
}

class ItemEntry {
	public String PC_PARTS_TITLE;
	public String  PC_PARTS_CODE;
	public String  PC_PARTS_MAKER; 
	public String  PC_PARTS_AVAIL; 
	public String PC_PARTS_DEC;
	
}
