package com.opeaklabs.rest.inventory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.GET;
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.QueryParam;

import org.codehaus.jettison.json.JSONArray;

import com.opeaklabs.dao.*;
import com.opeaklabs.rest.*;
import com.opeaklabs.util.ToJSON;
//jettison.jason.JASONArray;

@Path("/v1/inventory")
public class V1_inventory {
	Response rb = null;
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public Response returnTitle() {
		 rb = Response.ok("<p> About to return all parts </p>").build();
		return rb;
	}
	
	
	@Path("/allparts")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response returnAllParts() throws Exception {
		
		String returnString = null;
		PreparedStatement query = null;
		String myString = null;
		Connection conn = null;
		
		try {
			
			conn = RestSampleDao.restSampleConn().getConnection();
			query = conn.prepareStatement("select * from pc_parts");
			ResultSet rs = query.executeQuery();
/*			
			while (rs.next()) {
				myString = rs.getString(1);
				
			} */
			
			ToJSON toJson = new ToJSON();
			
			JSONArray jsonArray = new JSONArray();
			
			jsonArray = toJson.toJSONArray(rs);
			
			returnString = jsonArray.toString();
			
			rb = Response.ok(returnString).build();
			
			query.close();
			//returnString = " Total part count is  "+myString;

		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		finally {
			if (conn != null) {
				conn.close();	
			}
			
			
		}
		
		
		return rb;
	}
	

}
