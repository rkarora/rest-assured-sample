package com.opeaklabs.rest.inventory;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.QueryParam;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.opeaklabs.dao.*;
import com.opeaklabs.rest.*;
import com.opeaklabs.util.ToJSON;

@Path("/v3/inventory")
public class V3_inventory {
	
	// Shows how to use  POST with JSON consume ( for URL ENCODED input look at POST method in V2_investory.java 
	//http://localhost:8080/RestServiceSample/post.html
		
		@POST
		@Consumes({MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON})
		@Produces(MediaType.APPLICATION_JSON)
		public Response addPcParts(String incomingData) throws Exception {
			
			String returnString = null;
			JSONArray json = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			SQLQueries dao = new SQLQueries();
//			
			try {

				
				//ObjectMapper mapper = new ObjectMapper();
				// parse JSON data
				JSONObject partsData = new JSONObject(incomingData);
				
				System.out.println("JSON Incoming Data = "+partsData.toString());
				//ItemEntry itemEntry = mapper.readValue(incomingData,ItemEntry.class);
				
				int http_code = dao.insertIntoPC_parts(partsData.optString("PC_PARTS_TITLE"), 
						partsData.optString("PC_PARTS_CODE"), 
						partsData.optString("PC_PARTS_MAKER"), 
						partsData.optString("PC_PARTS_AVAIL"), 
						partsData.optString("PC_PARTS_DEC")); 
				
				if (http_code == 200) {
//					returnString = json.toString();
					jsonObject.put("HTTP_CODE", 200);
					jsonObject.put("MSG","item has been entered successfully");
					returnString = json.put(jsonObject).toString();
				}
				else {
					return Response.status(500).entity("Unable to inset record ").build();
				}
			
				System.out.println("retunr String  = "+returnString);		
			} catch (Exception e) {
				
				e.printStackTrace();
				return Response.status(500).entity("Server was unable to process your request ").build();
			}
			finally {
				
			}
			
			
			return Response.ok(returnString).build();
		}

		
		
		
		
		// Shows how to use  PUT  
		//Put is working fine with DB
		// http://localhost:8080/RestServiceSample/put.html
			
		    @Path("/{brand}/{item_number}")
			@PUT
			@Consumes({MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON})
			@Produces(MediaType.APPLICATION_JSON)
			public Response updateItem(@PathParam("brand")String brand, 
					                   @PathParam("item_number") String item_number, 
					                   String incomingData) 
					              throws Exception {
				
				int pk;
				int avail;
				int http_code;
				String returnString = null;
				JSONObject jsonObject = new JSONObject();
				JSONArray json = new JSONArray();
				SQLQueries dao = new SQLQueries();
//				
				try {

					
					//ObjectMapper mapper = new ObjectMapper();
					// parse JSON data
					
					System.out.println("INCOMING DATA = "+ incomingData);
					
					JSONObject partsData = new JSONObject(incomingData);
					pk = partsData.optInt("PC_PARTS_PK",0);
					avail = partsData.optInt("PC_PARTS_AVAIL",0);
					
					
					//System.out.println("JSON Incoming Data = "+partsData.toString());
					//ItemEntry itemEntry = mapper.readValue(incomingData,ItemEntry.class);
					
//					int http_code = dao.insertIntoPC_parts(partsData.optString("PC_PARTS_TITLE"), 
//							partsData.optString("PC_PARTS_CODE"), 
//							partsData.optString("PC_PARTS_MAKER"), 
//							partsData.optString("PC_PARTS_AVAIL"), 
//							partsData.optString("PC_PARTS_DEC")); 
					
//					
					int http_update_code = dao.updatePC_PARTS(pk,avail);
					if (http_update_code == 200) {
//						returnString = json.toString();
						jsonObject.put("HTTP_CODE", 200);
						jsonObject.put("MSG","item has been entered successfully");
						returnString = json.put(jsonObject).toString();
					}
					else {
						return Response.status(500).entity("Unable to inset record ").build();
					}
				
					System.out.println("retunr String  = "+returnString);		
				} catch (Exception e) {
					
					e.printStackTrace();
					return Response.status(500).entity("Server was unable to process your request ").build();
				}
				finally {
					
				}
				
				
				return Response.ok(returnString).build();
			}

			// Shows how to use  DELETE  
			//delete is working fine with DB
			// http://localhost:8080/RestServiceSample/delete.html
				
		    @Path("/{brand}/{item_number}")
			@DELETE
			@Consumes({MediaType.APPLICATION_FORM_URLENCODED,MediaType.APPLICATION_JSON})
			@Produces(MediaType.APPLICATION_JSON)
			public Response deleteItem(@PathParam("brand") String brand,
											@PathParam("item_number") int item_number,
											String incomingData) 
										throws Exception {
			    	
			    	String returnString = "all done";
			    	return Response.ok(returnString).build();
			    }
			    	//					
//					int pk;
//					int avail;
//					int http_code;
//					String returnString = null;
//					JSONObject jsonObject = new JSONObject();
//					JSONArray json = new JSONArray();
//					SQLQueries dao = new SQLQueries();
////					
//					try {
//
//						
//						//ObjectMapper mapper = new ObjectMapper();
//						// parse JSON data
//						
//						System.out.println("INCOMING DATA = "+ incomingData);
//						
//						JSONObject partsData = new JSONObject(incomingData);
//						pk = partsData.optInt("PC_PARTS_PK",0);
//				
////						
//						int http_update_code = dao.deletePC_PARTS(brand,item_number);
//						if (http_update_code == 200) {
////							returnString = json.toString();
//							jsonObject.put("HTTP_CODE", 200);
//							jsonObject.put("MSG","item has been entered successfully");
//							returnString = json.put(jsonObject).toString();
//						}
//						else {
//							return Response.status(500).entity("Unable to inset record ").build();
//						}
//					
//						System.out.println("retunr String  = "+returnString);		
//					} catch (Exception e) {
//						
//						e.printStackTrace();
//						return Response.status(500).entity("Server was unable to process your request ").build();
//					}
//					finally {
//						
//					}
//					
//					
//					return Response.ok(returnString).build();
//				}


}
