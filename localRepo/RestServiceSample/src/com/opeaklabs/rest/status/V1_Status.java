package com.opeaklabs.rest.status;
import javax.ws.rs.*;

import java.sql.*;

import javax.ws.rs.core.MediaType;

import com.opeaklabs.dao.RestSampleDao;

@Path("/v1/status/")
public class V1_Status {
	
	private static final String version = "1.0";

	@GET
	@Produces(MediaType.TEXT_HTML)
	public String returnTitle() {
		
		return "<p> Web  Service is running </p>";
	}
	
	@Path("/version")
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String returnVersion() {
		
		return "<p> Web  Service is running version </p>" + version;
	}
	@Path("/database")
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String returnDatabaseStatus() throws Exception {
	
		String returnString;
		PreparedStatement query = null;
		String myString = null;
		returnString = null;
		Connection conn = null;
		
		try {
			
			conn = RestSampleDao.restSampleConn().getConnection();
			query = conn.prepareStatement("select curtime()");
			ResultSet rs = query.executeQuery();
			
			while (rs.next()) {
				myString = rs.getString(1);
				
			}
			query.close();
			returnString = " Database Status "+myString;

		
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		finally {
			if (conn != null) {
				conn.close();	
			}
			
			
		}
		
		
		return returnString;
	}

}
