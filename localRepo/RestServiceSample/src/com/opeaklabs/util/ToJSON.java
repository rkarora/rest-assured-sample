package com.opeaklabs.util;

import org.owasp.esapi.*;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import java.sql.ResultSet;

public class ToJSON {
	
	public JSONArray toJSONArray(ResultSet rs) throws Exception {
		
		JSONArray ja = new JSONArray();
		String temp = null;
		try {
			
			java.sql.ResultSetMetaData rd = rs.getMetaData();
			
			while (rs.next()) {
				
				int numCol = rd.getColumnCount();
				System.out.println("No of colu are - "+numCol);
				
				JSONObject obj = new JSONObject();
				
				for (int i = 1; i <= numCol; i++) {
					String col_name = rd.getColumnName(i);
					System.out.println("No of col Name is - "+col_name);
					
					if (rd.getColumnType(i)==java.sql.Types.ARRAY) {
						
						obj.put(col_name, rs.getArray(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.BIGINT) {
						
						obj.put(col_name, rs.getInt(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.BOOLEAN) {
						
						obj.put(col_name, rs.getBoolean(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.BLOB) {
						
						obj.put(col_name, rs.getBlob(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.DOUBLE) {
						
						obj.put(col_name, rs.getDouble(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.FLOAT) {
						
						obj.put(col_name, rs.getFloat(col_name));
								 
					} 
					else if (rd.getColumnType(i)==java.sql.Types.INTEGER) {
						
						obj.put(col_name, rs.getInt(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.NVARCHAR) {
						
						obj.put(col_name, rs.getString(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.VARCHAR) {
						
						temp = rs.getString(col_name);
						temp = ESAPI.encoder().canonicalize(temp);
						temp = ESAPI.encoder().encodeForHTML(temp);
						obj.put(col_name, temp);
						
//						obj.put(col_name, rs.getString(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.TINYINT) {
						
						obj.put(col_name, rs.getInt(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.SMALLINT) {
						
						obj.put(col_name, rs.getInt(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.DATE) {
						
						obj.put(col_name, rs.getDate(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.TIMESTAMP) {
						
						obj.put(col_name, rs.getTimestamp(col_name));
								
					} 
					else if (rd.getColumnType(i)==java.sql.Types.NUMERIC) {
						
						obj.put(col_name, rs.getBigDecimal(col_name));
								
					} 
					else {
						
						obj.put(col_name, rs.getObject(col_name));
								
					} 
				
					
				}
				ja.put(obj);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ja;
	}

}
