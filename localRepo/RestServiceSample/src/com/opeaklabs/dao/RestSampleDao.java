package com.opeaklabs.dao;
import javax.naming.*;
import javax.sql.*;

import java.sql.Connection;
import java.sql.ResultSet;

public class RestSampleDao {
	
	private static DataSource restSample = null;
	private static Context context = null;
	
	public static DataSource restSampleConn() throws Exception {
		
		if (restSample != null) {
			
			return restSample;
		}
		
		try {
			 
			if (context == null) {
				context = new InitialContext();
				
			}
			System.out.println("Conext --->> "+context);
			restSample = (DataSource)context.lookup("java:comp/env/restsample");
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return restSample;
	}
	
	protected static Connection mysqlConnection() {
		
		
		
		
		Connection con = null;
		
//		con = RestSampleDao.restSampleConn().getConnection();
		
		
		try {
			con = restSampleConn().getConnection();
			return con;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return con;
		
	}

}
