package com.opeaklabs.dao;

import java.sql.*;

import org.codehaus.jettison.json.JSONArray;

import sun.tools.jar.resources.jar;

import com.opeaklabs.util.ToJSON;

public class SQLQueries extends RestSampleDao {
	
	
	public int insertIntoPC_parts(String pc_parts_title,String  pc_parts_code,String  pc_parts_maker, 
			                       String  pc_parts_avail, String pc_parts_dec ) throws Exception{
		
		PreparedStatement query = null;
		Connection conn	 = null;
		
		try {
			conn = mysqlConnection();

	
			query = conn.prepareStatement("insert into pc_parts" +
					" (pc_parts_title,pc_parts_code,pc_parts_maker,pc_parts_avail,"
					+ "pc_parts_dec) " + "values(?,?,?,?,?)");
			
			query.setString(1, pc_parts_title );
			query.setString(2, pc_parts_code );
			query.setString(3, pc_parts_maker );
			query.setString(4, pc_parts_avail );
			query.setString(5, pc_parts_dec);
			query.executeUpdate();
			
			
			query.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 500;
		} finally {
			
			if (conn != null) conn.close();
		}
		
		
		
		
		return 200;
	}

	
	
	
	public JSONArray getBrandParts(String brand) throws Exception {
	
	PreparedStatement query = null;
	Connection conn	 = null;
	
	ToJSON converter = new ToJSON();
	JSONArray json = new JSONArray();

	try {
		conn = mysqlConnection();

		//		pc_parts_pk int(11) AI PK 
//		select pc_parts_title,pc_parts_code,pc_parts_maker,pc_parts_avail,pc_parts_dec from pc_parts where 
//		
		query = conn.prepareStatement("select pc_parts_title,pc_parts_code,pc_parts_maker,pc_parts_avail,"
				+ "pc_parts_dec from pc_parts where upper(pc_parts_maker) = ? ");
		
		query.setString(1, brand.toUpperCase());
		ResultSet rs = query.executeQuery();
		
		json = converter.toJSONArray(rs);
		query.close();
	} catch (SQLException sqlError) {
		// TODO Auto-generated catch block
		sqlError.printStackTrace();
		return json;
	}
	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		return json;
	}
	finally {
		if (conn !=null) conn.close();
		
	}
	
	return json;
	
	}
	
	public JSONArray getSpecificPart(String brand, int part_code) throws Exception {
		
	PreparedStatement query = null;
	Connection conn	 = null;
	
	ToJSON converter = new ToJSON();
	JSONArray json = new JSONArray();

	try {
		conn = mysqlConnection();

		//		pc_parts_pk int(11) AI PK 
//		select pc_parts_title,pc_parts_code,pc_parts_maker,pc_parts_avail,pc_parts_dec from pc_parts where 
//		
		System.out.println("Brand from SQl = "+brand);
		query = conn.prepareStatement("select pc_parts_title,pc_parts_code,pc_parts_maker,pc_parts_avail,"
				+ "pc_parts_dec from pc_parts where upper(pc_parts_maker) = ? and pc_parts_pk = ?");
		
		query.setString(1, brand.toUpperCase());
		query.setInt(2, part_code);
		ResultSet rs = query.executeQuery();
		
		json = converter.toJSONArray(rs);
		query.close();
	} catch (SQLException sqlError) {
		// TODO Auto-generated catch block
		sqlError.printStackTrace();
		return json;
	}
	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		return json;
	}
	finally {
		if (conn !=null) conn.close();
		
	}
	
	return json;
	
	}

	public int updatePC_PARTS(int key, int available) throws SQLException {
		
		Connection conn	 = null;
		PreparedStatement newQuery = null;
		
		try {
		
			conn = mysqlConnection();
			System.out.println("Update data key = "+key + "  available  " +available);
			
			//System.out.println("Update data key2 = "+key + "  available  " +available);
			newQuery = conn.prepareStatement("update PC_PARTS " + 
						"set PC_PARTS_AVAIL = ? " + 
					    "where pc_parts_pk = ? ");
			//System.out.println("SQL Before : " + newQuery.toString());
//			newQuery = conn.prepareStatement(" update PC_PARTS set PC_PARTS_AVAIL = ? where pc_parts_pk = ?");

			System.out.println("SQL After : " + newQuery.toString());		
			newQuery.setInt(1, available);
			newQuery.setInt(2, key);
			
			System.out.println("SQL After : " + newQuery.toString());
			
			newQuery.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 500;
		}finally {
			
			if (conn != null) conn.close();
		}
		
		
		return 200;
	}
	
	public int deletePC_PARTS(String brand, String item_number) throws SQLException {
		
		Connection conn	 = null;
		PreparedStatement newQuery = null;
		
		try {
		
			conn = mysqlConnection();
			System.out.println("Delete data key = "+brand + "  available  " +item_number);
			
			//System.out.println("Update data key2 = "+key + "  available  " +available);
			newQuery = conn.prepareStatement("update PC_PARTS " + 
						"set PC_PARTS_AVAIL = ? " + 
					    "where pc_parts_pk = ? ");
			//System.out.println("SQL Before : " + newQuery.toString());
//			newQuery = conn.prepareStatement(" update PC_PARTS set PC_PARTS_AVAIL = ? where pc_parts_pk = ?");

			System.out.println("SQL After : " + newQuery.toString());		
			newQuery.setString(1, brand);
			newQuery.setString(2, item_number);
			
			System.out.println("SQL After : " + newQuery.toString());
			
			//newQuery.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 500;
		}finally {
			
			if (conn != null) conn.close();
		}
		
		
		return 200;
	}
	

	
}
